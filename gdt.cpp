// TODO: put gdt flags in an enum

class GDTEntry {
public:
	/* TODO: enable this when gcc supports it
	 * - switch from macro to constructor
	 * - uncomment "private:" on fields
	 * - use constructor in array literal
	 */
	/*constexpr GDTEntry(u32int base, u32int limit, u8int access, u8int gran) :
		limit_low(limit),
		base_low(base & 0xFFFFFF),
		access(access),
		granularity(gran),
		limit_high((limit >> 16) & 0xF),
		base_high((base >> 24) & 0xFF) {
	}*/
	
	#define GDT_ENTRY(BASE, LIMIT, ACCESS, GRAN) { \
		(LIMIT) & 0xFFFF, \
		(BASE) & 0xFFFFFF, \
		(ACCESS), \
		((LIMIT) >> 16) & 0x0F, \
		(GRAN), \
		((BASE) >> 24) & 0xFF \
	}

//private:
	u16int limit_low;
	u32int base_low : 24;
	u8int access;
	u8int limit_high : 4;
	u8int granularity : 4;
	u8int base_high;
} PACKED gdt[] = {
	GDT_ENTRY(0, 0, 0, 0), // required null segment
	GDT_ENTRY(0, 0xFFFFFFFF, 0x9A, 0xC), // kernel code segment
	GDT_ENTRY(0, 0xFFFFFFFF, 0x92, 0xC), // kernel data segment
	GDT_ENTRY(0, 0xFFFFFFFF, 0xFA, 0xC), // user code segment
	GDT_ENTRY(0, 0xFFFFFFFF, 0xF2, 0xC), // user data segment
};

extern u32int _kernel_offset;
struct GDTPtr {
	u16int limit;
	GDTEntry* base;
} PACKED gdt_ptr INIT_DATA = {
	sizeof(gdt),
	gdt
};
