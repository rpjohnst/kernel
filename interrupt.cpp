#include <interrupt.h>
#include <debug.h>
#include <io.h>

// stubs from isr.S
// TODO: find a less gross way to do this
extern "C" {
	extern void
	isr0(), isr1(), isr2(), isr3(), isr4(), isr5(), isr6(), isr7(),
	isr8(), isr9(), isr10(), isr11(), isr12(), isr13(), isr14(), isr15(),
	isr16(), isr17(), isr18(), isr19(), isr20(), isr21(), isr22(), isr23(),
	isr24(), isr25(), isr26(), isr27(), isr28(), isr29(), isr30(), isr31(),
	
	irq0(), irq1(), irq2(), irq3(), irq4(), irq5(), irq6(), irq7(),
	irq8(), irq9(), irq10(), irq11(), irq12(), irq13(), irq14(), irq15();
}

// TODO: put idt flags in an enum

class IDTEntry {
public:
	/* TODO: enable this when gcc supports it
	 * - switch from macro to constructor
	 * - uncomment "private:" on fields
	 * - use constructor in array literal
	 */
	/*constexpr IDTEntry(u32int base, u16int sel, u8int flags) :
		base_low(base),
		sel(sel),
		zero(0),
		flags(flags),
		base_high(base >> 16) {
	}*/
	
	#define IDT_ENTRY(BASE, SEL, FLAGS) { \
		u16int(BASE), \
		(SEL), \
		0, \
		(FLAGS), \
		u16int((BASE) >> 16) \
	}

//private:
	u16int base_low;
	u16int sel;
	u8int zero;
	u8int flags;
	u16int base_high;	
} PACKED idt[256] = {
	// TODO: put user mode rather than kernel mode in the flags

	#define ISR(NUM) IDT_ENTRY((u32int)isr##NUM, 0x08, 0x8E)
	ISR(0), ISR(1), ISR(2), ISR(3), ISR(4), ISR(5), ISR(6), ISR(7),
	ISR(8), ISR(9), ISR(10), ISR(11), ISR(12), ISR(13), ISR(14), ISR(15),
	ISR(16), ISR(17), ISR(18), ISR(19), ISR(20), ISR(21), ISR(22), ISR(23),
	ISR(24), ISR(25), ISR(26), ISR(27), ISR(28), ISR(29), ISR(30), ISR(31),

	#define IRQ(NUM) IDT_ENTRY((u32int)irq##NUM, 0x08, 0x8E)
	IRQ(0), IRQ(1), IRQ(2), IRQ(3), IRQ(4), IRQ(5), IRQ(6), IRQ(7),
	IRQ(8), IRQ(9), IRQ(10), IRQ(11), IRQ(12), IRQ(13), IRQ(14), IRQ(15)
};

// TODO: should this be moved/combined?
struct PIC {
	INIT PIC() {
		// TODO: label these ports
		outb(0x20, 0x11);
		outb(0xA0, 0x11);
		outb(0x21, 0x20);
		outb(0xA1, 0x28);
		outb(0x21, 0x04);
		outb(0xA1, 0x02);
		outb(0x21, 0x01);
		outb(0xA1, 0x01);
		outb(0x21, 0x0);
		outb(0xA1, 0x0);
	}
} pic;

extern u32int _kernel_offset;
struct IDTPtr {
	u16int limit;
	IDTEntry* base;
} PACKED idt_ptr INIT_DATA = {
	sizeof(idt),
	idt
};

static IsrPtr handlers[256];
void register_isr(u8int n, IsrPtr handler) {
	handlers[n] = handler;
}

static const char* interrupts[] = {
	"division by zero",
	"debug exception",
	"non-maskable interrupt",
	"breakpoint exception",
	"into detected overflow",
	"out of bounds exception",
	"invalid opcode exception",
	"no coprocessor",
	"double fault",
	"coprocessor segment overrun",
	"bad tss",
	"segment not present",
	"stack fault",
	"general protection fault",
	"page fault",
	"unknown interrupt",
	"coprocessor fault",
	"alignment check exception",
	"machine check exception"
};

/* this function get called from assembly, so we have to be careful
 * with -O3 (e.g. tail calls change how the caller has to clean up).
 * this means no variadic function calls at the end, and possibly
 * more which should be documented as it is discovered.
 */
extern "C" void isr_handler(Registers regs) {
	if (handlers[regs.int_no])
		handlers[regs.int_no](regs);
	else {
		if (regs.int_no <= 18)
			PANIC("unhandled %s\n", interrupts[regs.int_no]);
		else
			PANIC("unhandled interrupt %d\n", regs.int_no);
	}
	
	/* if this is an IRQ, signal EOI to the PIC
	 * IRQs 8-15 also need to signal the slave PIC
	 */
	if (regs.int_no >= 32 && regs.int_no <= 47) {
		if (regs.int_no >= 40)
			outb(0xA0, 0x20);
		outb(0x20, 0x20);
	}
}
