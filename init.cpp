#include <multiboot.h>
#include <memory.h>
#include <heap.h>
#include <debug.h>

u32int multiboot_magic INIT_DATA;
Multiboot::Info* mbi INIT_DATA;

// call static ctors in the .ctors section
// TODO: static destructors will be set here; provide for them?
typedef void (*CtorPtr)();
extern "C" INIT void do_global_ctors() {
	extern u32int __CTOR_NUM__;
	extern CtorPtr __CTOR_LIST__[];
	for (u32int c = 0; c < __CTOR_NUM__; c++) {
		__CTOR_LIST__[c]();
	}
}

/*
 * this is the main kernel entry point, called from the assembly
 * routine _start in startup.S. by this point, we should have:
 *  - created kernel page tables and enabled paging
 *  - initialized the kernel stack and heap
 *  - possibly set up other standard runtime support
 *  - run global constructors
 *  - loaded the GDT and IDT
 */
extern "C" INIT void system_startup() {
	// verify multiboot header
	if (multiboot_magic != Multiboot::Magic) {
		PANIC("not a multiboot-compliant loader");
		return;
	}
	DEBUG_TRACE("loaded by %s\n\n", mbi->boot_loader_name);
	
	/*u32int* a = new u32int[8];
	DEBUG_TRACE("a = 0x%x\n---\n", a);
	
	PageTable* b = new (aligned) PageTable;
	DEBUG_TRACE("b = 0x%x\n---\n", b);
	delete a;
	DEBUG_TRACE("a freed\n---\n");
	
	u32int* c = new u32int[32];
	DEBUG_TRACE("c = 0x%x\n---\n", c);
	delete b;
	DEBUG_TRACE("b freed\n---\n");
	
	PageTable* d = new (aligned) PageTable;
	DEBUG_TRACE("d = 0x%x\n---\n", d);
	
	PageTable* e = new (aligned) PageTable;
	DEBUG_TRACE("e = 0x%x\n---\n", e);*/
	
	asm ("sti");
}
