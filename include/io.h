#ifndef IO_H
#define IO_H

inline void outb(u16int port, u8int value) {
	asm ("outb %1, %0" :: "dN" (port), "a" (value));
}

inline u8int inb(u16int port) {
	u8int ret;
	asm ("inb %1, %0" : "=a" (ret) : "dN" (port));
	return ret;
}

inline void outw(u16int port, u16int value) {
	asm ("outw %1, %0" :: "dN" (port), "a" (value));
}

inline u16int inw(u16int port) {
	u16int ret;
	asm ("inw %1, %0" : "=a" (ret) : "dN" (port));
	return ret;
}

#endif
