#ifndef CONSOLE_H
#define CONSOLE_H

void clear();
void putchar(char c);
void itoa(char* buf, int base, int d);
void printf(const char* format, ...);

#ifndef DEBUG

#define DEBUG_TRACE(...)

#else

#define DEBUG_TRACE(...) do { \
	printf(__VA_ARGS__); \
} while (false)

#endif

#endif
