#ifndef TASK_H
#define TASK_H
#include <interrupt.h>
#include <list.h>
#include <memory.h>

struct Task : public List<Task> {
	Task(PageDirectory* space) :
		id(next_pid++), esp(0), ebp(0), eip(0), dir(space) {
	}
	
	u32int id;
	u32int esp, ebp, eip;
	PageDirectory* dir;

private:
	static u32int next_pid;
};

// TODO: user-level scheduling support
struct Scheduler {
	INIT Scheduler();
	int fork();
	u32int getpid() {
		return current->id;
	}
	
private:
	void switch_task(Task*);
	
	static int n;
	static char c;
	static char chars[4];
	static void tick(Registers);
	
	Task initial_task;
	
	// TODO: does the kernel even need a queue?
	Task* current;
	Task* ready;
	Task* last;
	
	u32int next_pid;
};

extern Scheduler scheduler;

// TODO: deal with multiple processors
inline int fork() {
	return scheduler.fork();
}

inline u32int getpid() {
	return scheduler.getpid();
}

#endif
