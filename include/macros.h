#define SECTION(X) __attribute__((section (X)))
#define ALIGNED(X) __attribute__((aligned (X)))
#define PACKED __attribute__((packed))

#define INIT SECTION(".init")
#define INIT_DATA SECTION(".init.data")

#define PANIC(...) do { \
	printf("kernel panic (%s:%d): ", __FILE__, __LINE__);\
	printf(__VA_ARGS__); \
	putchar('\n'); \
	asm ("cli; 1: hlt; jmp 1b"); \
} while (false)

#define ASSERT(C) if (!(C)) PANIC("assertion failed: " #C)
