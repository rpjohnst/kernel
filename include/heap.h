#ifndef HEAP_H
#define HEAP_H
#include <list.h>
#include <memory.h>

#define HEAP_MAGIC 0x1234321

class Heap {
public:
	INIT Heap(bool) {}
	Heap();
	void* alloc(size_t, bool = false);
	void free(void*);
	
private:
	struct Header;
	
	struct Footer {
		u32int magic;
		Header* header;
	};
	
	struct Header : public List<Header> {
		Header(bool f, size_t s) : magic(HEAP_MAGIC), free(f), size(s) {}
		
		u32int magic;
		bool free;
		size_t size;
		
		void* memory() {
			return reinterpret_cast<void*>(
				u32int(this) + sizeof(Header)
			);
		}
		
		Footer* footer() {
			return reinterpret_cast<Footer*>(
				u32int(this) + size - sizeof(Footer)
			);
		}
		
		void resize(size_t new_size) {
			size = new_size;
			*footer() = Footer{ HEAP_MAGIC, const_cast<Header*>(this) };
		}
	};
	
	Header* find_hole(size_t sz, bool align = false);
	void insert_hole(Header*);
	void remove_hole(Header*);
	
	Header* free_list;
	u8int heap[0x100000] __attribute__((aligned (PAGE_SIZE)));
};

extern Heap heap;

// standard
inline void* operator new(size_t size) { return heap.alloc(size, false); }
inline void* operator new[](size_t size) { return operator new(size); }
inline void operator delete(void* p) { heap.free(p); }
inline void operator delete[](void* p) { operator delete(p); }

// placement
inline void* operator new(size_t size, void* p) { return p; }
inline void* operator new[](size_t size, void* p) { return p; }
inline void operator delete(void*, void*) {}
inline void operator delete[](void*, void*) {}

// aligned
extern const struct Aligned {} aligned;
inline void* operator new(size_t size, const Aligned&) {
	return heap.alloc(size, true);
}
inline void* operator new[](size_t size, const Aligned&) {
	return operator new(size, aligned);
}

// physical
inline void* operator new(size_t size, u32int& phys) {
	void* mem = operator new(size);
	phys = current_directory->virt_to_phys(u32int(mem));
	return mem;
}
inline void* operator new[](size_t size, u32int& phys) {
	return operator new(size, phys);
}
inline void* operator new(size_t size, const Aligned&, u32int& phys) {
	void* mem = operator new(size, aligned);
	phys = current_directory->virt_to_phys(u32int(mem));
	return mem;
}
inline void* operator new[](size_t size, const Aligned&, u32int& phys) {
	return operator new(size, aligned, phys);
}

#endif
