#ifndef MEMORY_H
#define MEMORY_H
#include <multiboot.h>
#include <debug.h>

#define PAGE_SIZE 0x1000
#define LARGE_PAGE_SIZE 0x400000

#define PAGE_ALIGN(X) ((u32int(X) & ~0xFFF) + PAGE_SIZE)
#define PAGE_ALIGNED(X) (!(u32int(X) & 0xFFF))

extern u32int _kernel_offset,
_start_kernel_phys, _end_kernel_phys,
_start_kernel, _end_kernel,
_start_init, _end_init;

struct PageTable {
	struct Entry {
		Entry() {}
		
		explicit Entry(
			u32int f, bool p = false, bool w = false, bool u = false
		) :
			present(p), writeable(w), user(u),
			writethrough(false), nocache(false), accessed(false), dirty(false),
			zero(0), global(false), reserved(0), frame(f >> 12) {
		}
	
		bool present : 1;
		bool writeable : 1;
		bool user : 1;
		bool writethrough : 1;
		bool nocache : 1;
		bool accessed : 1;
		bool dirty : 1;
		u32int zero : 1;
		bool global : 1;
		u32int reserved : 3;
		u32int frame : 20;
	} pages[1024] ALIGNED(PAGE_SIZE);
};

// TODO: reflect changes to phys_tables in tables and vice versa - paging trick?
struct PageDirectory {
	/*
	 * we need a no-op constructor so we don't wind up clobbering
	 * the page directory built in init_paging when we run global
	 * constructors
	 */
	INIT explicit PageDirectory(bool) {}
	
	PageDirectory() {}
	
	PageTable::Entry& get_page(u32int address, bool make = true);
	u32int virt_to_phys(u32int address);
	PageDirectory* clone();
	
	struct Entry {
		Entry() {}
		
		explicit Entry(
			u32int f, bool p = false,
			bool w = false, bool u = false, bool l = false
		) :
			present(p), writeable(w), user(u),
			writethrough(false), nocache(false), accessed(false),
			zero(0), large(l), reserved(0), frame(f >> 12) {
		}
		
		bool present : 1;
		bool writeable : 1;
		bool user : 1;
		bool writethrough : 1;
		bool nocache : 1;
		bool accessed : 1;
		u32int zero : 1;
		bool large : 1;
		u32int reserved : 4;
		u32int frame : 20;
	} phys_tables[1024] ALIGNED(PAGE_SIZE);
	
	PageTable* tables[1024];
	u32int phys;
};

extern PageDirectory* current_directory;
inline void set_directory(PageDirectory& pd) {
	current_directory = &pd;
	asm ("mov %0, %%cr3" :: "r" (pd.phys));
}

#endif
