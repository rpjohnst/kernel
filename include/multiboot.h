#ifndef MULTIBOOT_H
#define MULTIBOOT_H

namespace Multiboot {

enum { Magic = 0x2BADB002 };

enum Flags : u32int {
	Memory = 0x001,
	BootDev = 0x002,
	CmdLine = 0x004,
	Mods = 0x008,
	AoutSyms = 0x010,
	ElfSHdr = 0x020,
	MemMap = 0x040,
	DriveInfo = 0x080,
	ConfigTable = 0x100,
	BootLoaderName = 0x200,
	APMTable = 0x400,
	VideoInfo = 0x800
};

struct AoutSymbols {
	u32int tabsize;
	u32int strsize;
	u32int addr;
	u32int reserved;
};

struct ElfSections {
	u32int num;
	u32int size;
	u32int addr;
	u32int shndx;
};

struct Module {
	u32int mod_start;
	u32int mod_end;
	
	u8int* cmdline;
	u32int pad;
};

struct MMap {
	u32int size;
	u64int addr;
	u64int length;
	u32int type;
	
	inline MMap* next() const {
		u32int next_addr = reinterpret_cast<u32int>(this) + size + sizeof(size);
		return reinterpret_cast<MMap*>(next_addr);
	}
};

struct Drive {
	u32int size;
	u8int number;
	u8int mode;
	u16int cylinders;
	u8int heads;
	u8int sectors;
	u16int ports[1]; // should be 0-sized
};
	
struct APM {
	u16int version;
	u16int cseg;
	u32int offset;
	u16int cseg_16;
	u16int dseg;
	u16int flags;
	u16int cseg_len;
	u16int cseg_16_len;
	u16int dseg_len;
};

struct Info {
	// indicates the validity of the other parts of this structure
	u32int flags;
	
	// upper and lower memory in kilobytes
	u32int mem_upper;
	u32int mem_lower;
	
	// BIOS drive and partitions
	u8int boot_device[4];
	
	// kernel command line
	u8int* cmdline;
	
	// bootloader-loaded modules
	u32int mods_count;
	Module* mods;
	
	union {
		AoutSymbols aout_syms;
		ElfSections elf_shdr;
	};
	
	// BIOS memory map
	size_t mmap_length;
	MMap* mmap;
	
	// hard drives
	size_t drives_length;
	Drive* drives;
	
	u32int config_table;
	
	u8int* boot_loader_name;
	
	APM* apm_table;
	
	u32int vbe_control_info;
	u32int vbe_mode_info;
	u16int vbe_mode;
	u16int vbe_interface_seg;
	u16int vbe_interface_off;
	u16int vbe_interface_len;
};

}

extern u32int multiboot_magic;
extern Multiboot::Info* mbi;

#endif
