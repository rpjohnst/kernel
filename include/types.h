typedef unsigned long long u64int;
typedef signed long long s64int;
typedef unsigned long u32int;
typedef signed long s32int;
typedef unsigned short u16int;
typedef signed short s16int;
typedef unsigned char u8int;
typedef signed char s8int;

typedef u32int size_t;

#define NULL 0
