#ifndef INTERRUPT_H
#define INTERRUPT_H

struct Registers {
	u32int ds;
	u32int edi, esi, ebp, esp, ebx, edx, ecx, eax;
	u32int int_no, err_code;
	u32int eip, cs, eflags, useresp, ss;
};

typedef void (*IsrPtr)(Registers);

// TODO: name the interrupts
void register_isr(u8int, IsrPtr);

#endif
