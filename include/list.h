#ifndef LIST_H
#define LIST_H

template <typename T>
struct List {
	void remove() {
		if (next) next->prev = prev;
		if (prev) prev->next = next;
	}
	
	void insert(T* l) {
		if (prev) prev->next = l;
		l->prev = prev;
		l->next = reinterpret_cast<T*>(this);
		prev = l;
	}
	
	T* prev;
	T* next;
};

#endif
