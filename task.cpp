#include <task.h>
#include <io.h>
#include <heap.h>

Scheduler scheduler;

u32int Task::next_pid = 1;

INIT Scheduler::Scheduler() :
	initial_task(current_directory),
	current(&initial_task), ready(current), last(current) {
	ready->prev = ready->next = NULL;
	
	register_isr(32, tick);
	
	/*
	 * initialize the PIT
	 * TODO: move this elsewhere for portability/SMP
	 * TODO: label these magic numbers
	 */
	outb(0x43, 0x34); // mode
	u32int d = 1193180 / 100; // 100Hz
	outb(0x40, d & 0xFF);
	outb(0x40, d >> 8);
}
	
int Scheduler::fork() {
	asm ("cli");
	
	Task* parent = current;
	Task* child = new Task(current_directory->clone());
	parent->next->insert(child);
	
	/*
	 * the child process should start out in this function's stack frame,
	 * but skip past the task switching code to the child_return code
	 */
	asm ("mov %%esp, %0" : "=r"(child->esp));
	asm ("mov %%ebp, %0" : "=r"(child->ebp));
	child->eip = u32int(&&child_return);
	
	/*
	 * switching tasks jumps to child_return below, and when we get switched
	 * back to we resume after the call to switch_task
	 */
	switch_task(child);
	return child->id;
	
child_return:
	asm ("sti");
	return 0;
}

/*
 * because this function is in the kernel it is shared accross address spaces
 * generally, a process's eip will be in this function (it could also be in
 * fork(), but that's only once)
 *
 * this function switches tasks by:
 *  - saving the old process's context so that it's ready to be switched to
 *    later using this function
 *  - switching stacks and address spaces to the new process
 *  - returning to the new process, using the new stack. this means that it
 *    will generally return from the new process' last call to switch task
 */
void Scheduler::switch_task(Task* next) {
	u32int esp, ebp, eip;
	asm ("mov %%esp, %0" : "=r"(esp));
	asm ("mov %%ebp, %0" : "=r"(ebp));
	eip = u32int(&&resume_return);
	
	current->esp = esp;
	current->ebp = ebp;
	current->eip = eip;
	
	current = next;
	esp = current->esp;
	ebp = current->ebp;
	
	asm (" \
		cli; \
		mov %0, %%esp; \
		mov %1, %%ebp; \
		mov %2, %%cr3; \
		sti"
	:: "r"(esp), "r"(ebp), "r"(current_directory->phys));
	
resume_return:
	return;
}

// TODO: figure out an SMP-compatible way of doing this
void Scheduler::tick(Registers) {
	if (++n % 100 == 0)
		((char*)0xB8000)[158] = chars[++c % sizeof(chars)];
	
	// TODO: user-space scheduler support here?
	Task* next = scheduler.current->next;
	if (next == NULL) next = scheduler.ready;
	scheduler.switch_task(next);
}

int Scheduler::n = 0;
char Scheduler::c = 0;
char Scheduler::chars[] = { '/', '-', '\\', '|' };
