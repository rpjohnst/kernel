#include <heap.h>
#include <debug.h>

u8int initial_stack[0x4000] INIT_DATA;

extern "C" INIT void init_heap() {
	// placement new just runs the constructor at the given address
	new (&heap) Heap;
}

/*
 * use the no-op constructor overload here so we can explicitly call the
 * actual constructor when we want to and not in the big cloud of pretty-
 * much indeterminate ordering of global constructors
 */
Heap heap(true);

Heap::Heap() : free_list(reinterpret_cast<Header*>(heap)) {
	*free_list = Header(true, sizeof(heap));
	free_list->prev = free_list->next = NULL;
}

/*
 * size is the actual size given back to the caller
 * required is size + sizeof(header) + sizeof(footer)
 *
 * TODO: expand/contract? physical address?
 */
void* Heap::alloc(size_t size, bool align) {
	// find a big enough hole
	size_t required = size + sizeof(Header) + sizeof(Footer);
	Header* block = find_hole(required, align);
	
	DEBUG_TRACE("alloc: 0x%x | 0x%x / 0x%x\n", block, required, block->size);
	if (!block) PANIC("out of memory");
	
	/*
	 * align the hole if necessary by:
	 *  - removing the hole
	 *  - slicing the new block out of it
	 *  - re-inserting the leftover chunk at the correct position in the list
	 *  - updating block to point to the new hole
	 */
	if (align && !PAGE_ALIGNED(block->memory())) {
		remove_hole(block);
		
		Header* new_block = reinterpret_cast<Header*>(
			PAGE_ALIGN(block->memory()) - sizeof(Header)
		);
		*new_block = Header(false, 0);
		new_block->resize(block->size - (u32int(new_block) - u32int(block)));
		
		block->resize(u32int(new_block) - u32int(block));
		insert_hole(block);
		
		block = new_block;
	}
	
	// take the hole out of the free list and mark it as used
	if (free_list == block)
		free_list = free_list->next;
	block->remove();
	block->free = false;
	
	/*
	 * if the hole's too small, just take the rest too
	 * if it's too big, split it up
	 */
	if (block->size - required < sizeof(Header) + sizeof(Footer)) {
		size += block->size - required;
		required = block->size;
	}
	else if (block->size - required > 0) {
		// shrink this hole
		size_t old_size = block->size;
		block->resize(required);
		
		// insert a new hole after
		Header* new_hole = reinterpret_cast<Header*>(
			u32int(block->footer()) + sizeof(Footer)
		);
		*new_hole = Header(true, 0);
		new_hole->resize(old_size - block->size);
		insert_hole(new_hole);
	}
	
	return block->memory();
}

void Heap::free(void* p) {
	if (p == NULL) return;
	
	Header* block = reinterpret_cast<Header*>(
		u32int(p) - sizeof(Header)
	);
	ASSERT(block->magic == HEAP_MAGIC);
	ASSERT(block->footer()->magic == HEAP_MAGIC);
	block->free = true;
	
	// unify left?
	Footer* test_footer = reinterpret_cast<Footer*>(
		u32int(block) - sizeof(Footer)
	);
	if (test_footer->magic == HEAP_MAGIC && test_footer->header->free) {
		remove_hole(test_footer->header);
		
		u32int size = block->size;
		block = test_footer->header;
		block->resize(block->size + size);
	}
	
	// unify right?
	Header* test_header = reinterpret_cast<Header*>(
		u32int(block) + block->size
	);
	if (test_header->magic == HEAP_MAGIC && test_header->free) {
		remove_hole(test_header);
		
		block->resize(block->size + test_header->size);
	}
	
	// add the possibly-bigger block back into the free_list
	insert_hole(block);
}

Heap::Header* Heap::find_hole(size_t size, bool align) {
	for (Header* h = free_list; h != NULL; h = h->next) {
		u32int available = h->size;
		DEBUG_TRACE("(0x%x, 0x%x) -> 0x%x\n", h, available, h->next);
		
		if (align && !PAGE_ALIGNED(h->memory())) {
			u32int offset = PAGE_ALIGN(h->memory()) - sizeof(Header) - u32int(h);
			
			if (offset < available)
				available -= offset;
			else
				continue;
		}
		
		if (available >= size)
			return h;
	}
	
	return NULL;
}

void Heap::insert_hole(Header* hole) {
	if (free_list == NULL) {
		free_list = hole;
		hole->prev = hole->next = NULL;
	}
	
	Header* prev = NULL;
	for (Header* h = free_list; h != NULL; h = h->next) {
		if (h->size > hole->size) {
			h->insert(hole);
			if (free_list == h)
				free_list = hole;
			
			return;
		}
		
		prev = h;
	}
	
	if (prev != NULL) {
		prev->next = hole;
		hole->prev = prev;
		hole->next = NULL;
		return;
	}
}

void Heap::remove_hole(Header* hole) {
	hole->remove();
	if (free_list == hole)
		free_list = free_list->next;
}
