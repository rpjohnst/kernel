#include <interrupt.h>
#include <io.h>
#include <debug.h>

class Keyboard {
public:
	INIT Keyboard() {
		register_isr(33, keypress);
	}

private:
	static void keypress(Registers) {
		u8int scancode = inb(0x60);
		putchar(scancode);
	}
} keyboard;
