#include <debug.h>
#include <io.h>

struct Console {
	INIT Console() { clear(); }
} console;

static const int columns = 80, rows = 24;
static u8int xpos = 0, ypos = 0;
static u8int attribute = 0x07;

struct Char {
	u8int character, attribute;
	Char(char c, u8int attr = ::attribute) : character(c), attribute(attr) {}
};

static Char* const video = (Char*)0xB8000;

static void move_cursor() {
	u16int loc = ypos * columns + xpos;
	outb(0x3D4, 14);
	outb(0x3D5, loc >> 8);
	outb(0x3D4, 15);
	outb(0x3D5, loc);
}

static void scroll() {
	if (ypos >= rows) {
		for (int i = 0; i < (columns - 1) * rows; i++)
			video[i] = video[i + columns];
		
		for (int i = (columns - 1) * rows; i < columns * rows; i++)
			video[i] = ' ';
		
		ypos = rows - 1;
	}
}

void clear() {
	for (int i = 0; i < columns * rows; i++)
		video[i] = ' ';
	
	xpos = ypos = 0;
	move_cursor();
}

void putchar(char c) {
	switch (c) {
		case '\t': xpos = (xpos + 8) & ~(8 - 1); break;
		case '\r': xpos = 0; break;
		case '\n': xpos = 0; ypos++; break;
		case '\b':
			video[ypos * columns + xpos] = ' ';
			if (xpos > 0)
				xpos--;
			else {
				xpos = columns - 1;
				ypos--;
			}
			break;
		default:
			video[ypos * columns + xpos] = c;
			xpos++;
			break;
	}
	
	if (xpos >= columns) {
		xpos = 0;
		ypos++;
	}
	
	scroll();
	move_cursor();
}

void itoa(char* buf, int base, int d) {
	unsigned int ud = d;
	int divisor = 10;
	
	char* p = buf;
	if (base == 'd' && d < 0) {
		*p++ = '-';
		ud = -d;
	}
	else if (base == 'x')
		divisor = 16;
	
	do {
		int remainder = ud % divisor;
		*p++ = remainder < 10 ? '0' + remainder : 'A' + remainder - 10;
	} while (ud /= divisor);
	*p = 0;
	
	char* p1 = buf;
	char* p2 = p - 1;
	while (p1 < p2) {
		char tmp = *p1;
		*p1 = *p2;
		*p2 = tmp;
		
		p1++;
		p2--;
	}
}

void printf(const char* format, ...) {
	char** arg = (char**)&format + 1;
	
	int c;
	char buf[20];
	while ((c = *format++) != 0) {
		if (c != '%') putchar(c);
		else {
			const char* p;
			
			c = *format++;
			switch (c) {
				case 'd':
				case 'u':
				case 'x':
					itoa(buf, c, *(int*)arg++);
					p = buf;
					goto string;
				
				case 's':
					p = *arg++;
					if (!p) p = "(null)";
				
				string:
					while (*p) putchar(*p++);
					break;
				
				default: putchar(*(int*)arg++);
			}
		}
	}
}
