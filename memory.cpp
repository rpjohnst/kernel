#include <memory.h>
#include <debug.h>
#include <multiboot.h>
#include <interrupt.h>
#include <heap.h>

PageDirectory kernel_directory(true);
PageDirectory initial_directory(true);
PageDirectory* current_directory;

PageTable::Entry& PageDirectory::get_page(u32int address, bool make) {
	u32int index = address / 0x1000 / 1024;
	
	if (make && tables[index] == NULL) {
		u32int phys;
		tables[index] = new (aligned, phys) PageTable;
		phys_tables[index] = Entry(phys);
	}
	
	return tables[index]->pages[address / 0x1000 % 1024];
}

u32int PageDirectory::virt_to_phys(u32int address) {
	u32int index = address / 0x1000 / 1024;
	
	if (phys_tables[index].large)
		return (phys_tables[index].frame << 12) | (address & 0x3FFFFF);
	else
		return (get_page(address, false).frame << 12) | (address & 0xFFF);
}

PageDirectory* PageDirectory::clone() {
	u32int phys;
	PageDirectory* dir = new (aligned, phys) PageDirectory;
	dir->phys = phys;
	
	for (u32int i = 0; i < 1024; i++) {
		if (tables[i] == NULL) continue;
		
		if (tables[i] == kernel_directory.tables[i]) {
			dir->tables[i] = tables[i];
			dir->phys_tables[i] = phys_tables[i];
		}
		else
			PANIC("copy not implemented yet");
	}
	
	return dir;
}

/*
 * this routine maps in the kernel so the rest of the system can be
 * bootstrapped. kernel_directory is the main kernel, and holds the pages
 * that will be shared by every address space. initial_directory will
 * also include the init code and kernel stack, which will not be shared
 */
extern "C" INIT void init_paging() {
	/*
	 * we need to access these three variables before enabling paging.
	 * however, they aren't mapped in yet, so we have to access them
	 * at their physical addresses
	 */
	PageDirectory& kdir = *reinterpret_cast<PageDirectory*>(
		u32int(&kernel_directory) - u32int(&_kernel_offset)
	);
	PageDirectory& idir = *reinterpret_cast<PageDirectory*>(
		u32int(&initial_directory) - u32int(&_kernel_offset)
	);
	PageDirectory*& cdir = *reinterpret_cast<PageDirectory**>(
		u32int(&current_directory) - u32int(&_kernel_offset)
	);
	
	// first map in the kernel proper to kernel_directory
	u32int vaddr = u32int(&_start_kernel), paddr = u32int(&_start_kernel_phys);
	while (vaddr < u32int(&_end_kernel)) {
		u32int i = vaddr / LARGE_PAGE_SIZE;
		kdir.phys_tables[i] = PageDirectory::Entry(paddr, true, true, false, true);
		kdir.tables[i] = reinterpret_cast<PageTable*>(vaddr);
		
		vaddr += LARGE_PAGE_SIZE;
		paddr += LARGE_PAGE_SIZE;
	}
	
	// now copy it over to inital_directory and map in the init section as well
	idir = kdir;
	for (u32int addr = 0; addr < u32int(mbi->mmap) + mbi->mmap_length; addr += LARGE_PAGE_SIZE) {
		u32int i = addr / LARGE_PAGE_SIZE;
		idir.phys_tables[i] = PageDirectory::Entry(addr, true, true, false, true);
		idir.tables[i] = reinterpret_cast<PageTable*>(addr);
	}
	
	idir.phys = u32int(&idir);
	kdir.phys = u32int(&kdir);
	
	// we can't use set_directory to set current_directory
	cdir = &initial_directory;
	asm ("mov %0, %%cr3" :: "r" (idir.phys));
}

struct Memory {
	INIT Memory() {
		// TODO: store available pages
		if (!(mbi->flags & Multiboot::Memory))
			PANIC("no memory map passed from loader");
		
		for (
			Multiboot::MMap* mmap = mbi->mmap;
			u32int(mmap) < u32int(mbi->mmap) + mbi->mmap_length;
			mmap = mmap->next()
		) {
			printf(
				"0x%x - 0x%x : %d\n",
				(u32int)mmap->addr,
				(u32int)(mmap->addr + mmap->length),
				mmap->type
			);
		}
		printf("\n");
		
		register_isr(14, page_fault);
	}
	
	static void page_fault(Registers) {
		u32int address;
		asm ("mov %%cr2, %0" : "=r" (address));
		
		PANIC("page fault (0x%x)", address);
	}
} memory;

// TODO: find a permanent home and don't just stick it in init
extern "C" INIT void* memcpy(u8int* dest, u8int* src, size_t len) {
	void* pos = dest;
	while (len-- > 0) *dest++ = *src++;
	return pos;
}
